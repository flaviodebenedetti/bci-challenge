-- Creación de la tabla UserEntity
-- CREATE TABLE usuario (
--     id UUID PRIMARY KEY,
--     name VARCHAR(255),
--     email VARCHAR(255) UNIQUE NOT NULL,
--     password VARCHAR(255),
--     created TIMESTAMP,
--     modified TIMESTAMP,
--     last_login TIMESTAMP,
--     token VARCHAR(255),
--     is_active BOOLEAN
-- );

-- Creación de la tabla PhoneEntity
-- CREATE TABLE telefonos (
--     id SERIAL PRIMARY KEY,
--     number VARCHAR(255),
--     citycode VARCHAR(255),
--     countrycode VARCHAR(255),
--     user_id UUID REFERENCES usuario(id)
-- );

-- Inserciones para la tabla usuario (UserEntity)
INSERT INTO usuario (id, name, email, password, created, modified, last_login, token, is_active)
VALUES
    (RANDOM_UUID(), 'Lionel', 'lionel@messi.com', 'Contr@sena123', '01-01-2022 00:00:00', '01-01-2022 00:00:00', '01-01-2022 00:00:00', 'token123', true),
    (RANDOM_UUID(), 'Rhaenyra', 'rhaenyra@targaryen.com', 'Contr@sena123', '02-01-2022 00:00:00', '02-01-2022 00:00:00', '02-01-2022 00:00:00', 'token456', true),
    (RANDOM_UUID(), 'Daemon', 'daemon@targaryen.com', 'Contr@sena123', '02-01-2022 00:00:00', '02-01-2022 00:00:00', '02-01-2022 00:00:00', 'token456', true),
    (RANDOM_UUID(), 'Ragnar', 'ragnar@lothbrok.com', 'Contr@sena123', '02-01-2022 00:00:00', '02-01-2022 00:00:00', '02-01-2022 00:00:00', 'token456', true),
    (RANDOM_UUID(), 'Lagertha', 'lagertha@valquiria.com', 'Contr@sena123', '02-01-2022 00:00:00', '02-01-2022 00:00:00', '02-01-2022 00:00:00', 'token456', true);

-- Inserciones para la tabla phone_entity (PhoneEntity)
INSERT INTO telefonos (id, number, citycode, countrycode, user_id)
VALUES
    (1, '123456789', '123', '456', (SELECT id FROM usuario WHERE email = 'lionel@messi.com')),
    (2, '987654321', '987', '654', (SELECT id FROM usuario WHERE email = 'rhaenyra@targaryen.com')),
    (3, '987654321', '789', '456', (SELECT id FROM usuario WHERE email = 'daemon@targaryen.com')),
    (4, '987654321', '321', '234', (SELECT id FROM usuario WHERE email = 'ragnar@lothbrok.com')),
    (5, '987654321', '567', '321', (SELECT id FROM usuario WHERE email = 'lagertha@valquiria.com'));





