package com.bci.challenge.infrastructure.repository;

import com.bci.challenge.domain.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, String> {
    @Query(value = "SELECT * FROM usuario WHERE email = :email", nativeQuery = true)
    List findByEmail(@Param("email") String email);
    @Query(value = "SELECT * FROM usuario WHERE email = :email AND password = :password", nativeQuery = true)
    UserEntity findByEmailAndPassword(@Param("email") String email, @Param("password") String password);
}
