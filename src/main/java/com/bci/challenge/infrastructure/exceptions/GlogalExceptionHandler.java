package com.bci.challenge.infrastructure.exceptions;

import com.bci.challenge.domain.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class GlogalExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponse> handlerMethodArgumentNotValidException(MethodArgumentNotValidException exception, WebRequest webRequest){
        Map<String, String> mapErrors = new HashMap<>();
        exception.getBindingResult().getAllErrors().forEach((error) -> {
            String key = ((FieldError) error).getField();
            String value = error.getDefaultMessage();
            mapErrors.put(key, value);
        });
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(mapErrors.toString()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidEmailFormatException.class)
    public ResponseEntity<ErrorResponse> handlerInvalidEmailFormatException(InvalidEmailFormatException exception, WebRequest webRequest){
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EmailAlreadyExistsException.class)
    public ResponseEntity<ErrorResponse> handlerEmailAlreadyExistsException(EmailAlreadyExistsException exception, WebRequest webRequest){
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FindUserByIdNotFoundException.class)
    public ResponseEntity<ErrorResponse> handlerFindUserByIdNotFoundException(FindUserByIdNotFoundException exception, WebRequest webRequest){
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(FindUsersException.class)
    public ResponseEntity<ErrorResponse> handlerFindUsersException(FindUsersException exception, WebRequest webRequest){
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(InvalidPasswordException.class)
    public ResponseEntity<ErrorResponse> handlerInvalidPasswordException(InvalidPasswordException exception, WebRequest webRequest){
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }
}
