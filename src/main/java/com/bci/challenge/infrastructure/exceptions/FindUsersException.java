package com.bci.challenge.infrastructure.exceptions;

import org.springframework.http.HttpStatus;

public class FindUsersException extends RuntimeException {

    private String code;
    private HttpStatus status;

    public FindUsersException(String code, HttpStatus httpStatus) {
        super("No hay usuarios registrados");
        this.code = code;
        this.status = httpStatus;
    }
}
