package com.bci.challenge.infrastructure.exceptions;

import org.springframework.http.HttpStatus;

public class InvalidPasswordException extends RuntimeException{

    private String code;
    private HttpStatus status;

    public InvalidPasswordException(String code, HttpStatus httpStatus){
        super("Formato de contraseña inválido");
        this.code = code;
        this.status = httpStatus;
    }
}
