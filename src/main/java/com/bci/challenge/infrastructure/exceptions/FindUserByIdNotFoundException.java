package com.bci.challenge.infrastructure.exceptions;

import org.springframework.http.HttpStatus;

public class FindUserByIdNotFoundException extends RuntimeException{

    private String code;
    private HttpStatus status;

    public FindUserByIdNotFoundException(String code, HttpStatus httpStatus){
        super("El usuario no fue encontrado");
        this.code = code;
        this.status = httpStatus;
    }
}
