package com.bci.challenge.infrastructure.exceptions;

import org.springframework.http.HttpStatus;

public class InvalidEmailFormatException extends RuntimeException{

    private String code;
    private HttpStatus status;

    public InvalidEmailFormatException(String code, HttpStatus httpStatus){
        super("Formato de email invalido");
        this.code = code;
        this.status = httpStatus;
    }
}
