package com.bci.challenge.infrastructure.exceptions;

import org.springframework.http.HttpStatus;

public class EmailAlreadyExistsException extends Throwable {
    private String code;
    private HttpStatus status;

    public EmailAlreadyExistsException(String code, HttpStatus httpStatus){
        super("Este email ya existente");
        this.code = code;
        this.status = httpStatus;
    }
}
