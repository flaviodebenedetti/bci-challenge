package com.bci.challenge.application.controllers;

import com.bci.challenge.application.usecases.UserUseCase;
import com.bci.challenge.domain.dto.UserDto;
import com.bci.challenge.domain.request.LoginRequest;
import com.bci.challenge.domain.request.UpdateUserRequest;
import com.bci.challenge.domain.request.UserRequest;
import com.bci.challenge.domain.response.UserResponse;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@AllArgsConstructor
public class UserController {

    private final UserUseCase useCase;

    @PostMapping("/register")
    public ResponseEntity registerUser(@RequestBody @Valid UserRequest request) {
        UserResponse response = useCase.registerUser(request);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/login")
    public ResponseEntity loginUser(@RequestBody @Valid LoginRequest loginRequest) {
        UserResponse response = useCase.loginUser(loginRequest);
        return ResponseEntity.ok(response);
    }

    @GetMapping()
    public ResponseEntity<?> findAll(){
        List<UserDto> users = useCase.findAll();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") String id){
        return new ResponseEntity<>(useCase.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") String id){
        return new ResponseEntity<>(useCase.deleteUser(id), HttpStatus.OK);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<?> updateUser(@RequestBody @Valid UpdateUserRequest updateUserRequest, @PathVariable("id") String id){
        UserResponse response = useCase.updateUser(id, updateUserRequest);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
