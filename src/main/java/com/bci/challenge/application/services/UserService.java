package com.bci.challenge.application.services;

import com.bci.challenge.application.usecases.UserUseCase;
import com.bci.challenge.config.ValidationConfig;
import com.bci.challenge.domain.dto.UserDto;
import com.bci.challenge.domain.entities.PhoneEntity;
import com.bci.challenge.domain.entities.UserEntity;
import com.bci.challenge.domain.request.*;
import com.bci.challenge.domain.response.UserResponse;
import com.bci.challenge.infrastructure.exceptions.EmailAlreadyExistsException;
import com.bci.challenge.infrastructure.exceptions.FindUserByIdNotFoundException;
import com.bci.challenge.infrastructure.exceptions.InvalidPasswordException;
import com.bci.challenge.infrastructure.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserService implements UserUseCase {

    private final UserRepository repository;
    private final ValidationConfig validationConfig;

    @Override
    public UserResponse registerUser(UserRequest request) {
        validatePassword(request.password());
        isEmailExist(request.email());
        UserEntity user = getUserForSave(request);
        repository.save(user);
        return UserResponse.registerOkResponse(user.toUserDto());
    }

    @Override
    public UserResponse loginUser(LoginRequest request) {
        UserEntity user = repository.findByEmailAndPassword(request.email(), request.password());
        if(user == null){
            throw new FindUserByIdNotFoundException(HttpStatus.NO_CONTENT.toString(), HttpStatus.NO_CONTENT);
        }
        updateLastLogin(user);
        return UserResponse.loginOkResponse(user.getName());
    }

    @Override
    public List<UserDto> findAll() {
        List<UserEntity> listUserEntity = repository.findAll();
        if(listUserEntity.isEmpty()){
            throw new FindUserByIdNotFoundException(HttpStatus.NO_CONTENT.toString(), HttpStatus.NO_CONTENT);
        }

        List<UserDto> list = new ArrayList<>();
        for (UserEntity userEntity: listUserEntity) {
            list.add(userEntity.toUserDtoAllAttributes());
        }
        return list;
    }

    @Override
    public UserResponse findById(String id) {
        Optional<UserEntity> optionalUserEntity = repository.findById(id);
        if(optionalUserEntity.isEmpty()){
            throw new FindUserByIdNotFoundException(HttpStatus.NO_CONTENT.toString(), HttpStatus.NO_CONTENT);
        }
        return UserResponse.findUserOkResponse(optionalUserEntity.get().toUserDtoAllAttributes());
    }

    @Override
    public UserResponse deleteUser(String id) {
        Optional<UserEntity> optionalUserEntity = repository.findById(id);
        if(optionalUserEntity.isEmpty()){
            throw new FindUserByIdNotFoundException(HttpStatus.NO_CONTENT.toString(), HttpStatus.NO_CONTENT);
        }
        repository.deleteById(id);
        return UserResponse.deleteOkResponse();
    }

    @Override
    public UserResponse updateUser(String id, UpdateUserRequest updateUserRequest) {
        Optional<UserEntity> optionalUserEntity = repository.findById(id);
        if(optionalUserEntity.isEmpty()){
            throw new FindUserByIdNotFoundException(HttpStatus.NO_CONTENT.toString(), HttpStatus.NO_CONTENT);
        }

        UserEntity user = getUserUpdate(optionalUserEntity.get(), updateUserRequest);
        repository.save(user);
        return UserResponse.updateOkResponse();
    }

    private UserEntity getUserUpdate(UserEntity userEntity, UpdateUserRequest updateUserRequest) {
        boolean flag = false;

        if(updateUserRequest.name()!=null){
            userEntity.setName(updateUserRequest.name());
            flag = true;
        }
        if(updateUserRequest.email()!=null){
            isEmailExist(updateUserRequest.email());
            userEntity.setEmail(updateUserRequest.email());
            flag = true;
        }
        if(updateUserRequest.isActive()!=null){
            userEntity.setActive(updateUserRequest.isActive());
            flag = true;
        }
        if(updateUserRequest.password()!=null){
            validatePassword(updateUserRequest.password());
            userEntity.setPassword(updateUserRequest.password());
            flag = true;
        }
        if(updateUserRequest.token()!=null){
            userEntity.setToken(updateUserRequest.token());
            flag = true;
        }
        if(updateUserRequest.phones()!=null){
            List<PhoneEntity> list = getPhoneUpdate(updateUserRequest.phones());
            userEntity.setPhones(list);
            flag = true;
        }
        if(flag){
            userEntity.setModified(formatterDate(LocalDateTime.now()));
        }

        return userEntity;
    }

    private void updateLastLogin(UserEntity user) {
        user.setLastLogin(formatterDate(LocalDateTime.now()));
        repository.save(user);
    }

    private UserEntity getUserForSave(UserRequest request) {
        UserEntity userEntity = new UserEntity();
        userEntity.setId(UUID.randomUUID().toString());
        userEntity.setActive(true);
        userEntity.setEmail(request.email());
        userEntity.setPassword(request.password());
        userEntity.setPhones(getPhone(request.phones()));
        userEntity.setName(request.name());
        userEntity.setCreated(formatterDate(LocalDateTime.now()));
        userEntity.setModified(formatterDate(LocalDateTime.now()));
        userEntity.setLastLogin(formatterDate(LocalDateTime.now()));
        userEntity.setToken(UUID.randomUUID().toString());
        return userEntity;
    }

    private String formatterDate(LocalDateTime now) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formattedDateTime = now.format(formatter);
        return formattedDateTime;
    }

    private List<PhoneEntity> getPhoneUpdate(List<UpdatePhoneRequest> phones) {
        List<PhoneEntity> phoneEntities = new ArrayList<>();
        for (UpdatePhoneRequest phone: phones) {
            phoneEntities.add(PhoneEntity.builder()
                    .id(UUID.randomUUID().getLeastSignificantBits())
                    .number(phone.number())
                    .countrycode(phone.countrycode())
                    .citycode(phone.citycode())
                    .build());
        }
        return phoneEntities;
    }

    private List<PhoneEntity> getPhone(List<PhoneRequest> phoneRequests) {
        List<PhoneEntity> phoneEntities = new ArrayList<>();
        for (PhoneRequest phone: phoneRequests) {
            phoneEntities.add(PhoneEntity.builder()
                    .id(UUID.randomUUID().getLeastSignificantBits())
                    .number(phone.number())
                    .countrycode(phone.countrycode())
                    .citycode(phone.citycode())
                    .build());
        }
        return phoneEntities;
    }

    @SneakyThrows
    private void isEmailExist(String email) {
        if(repository.findByEmail(email).size() > 0){
            throw new EmailAlreadyExistsException(HttpStatus.BAD_REQUEST.toString(), HttpStatus.BAD_REQUEST);
        }
    }

    private void validatePassword(String password){
        if(!password.matches(validationConfig.getPasswordRegex())){
            throw new InvalidPasswordException(HttpStatus.BAD_REQUEST.toString(), HttpStatus.BAD_REQUEST);
        }
    }

}
