package com.bci.challenge.application.usecases;

import com.bci.challenge.domain.dto.UserDto;
import com.bci.challenge.domain.request.LoginRequest;
import com.bci.challenge.domain.request.UpdateUserRequest;
import com.bci.challenge.domain.request.UserRequest;
import com.bci.challenge.domain.response.UserResponse;

import java.util.List;

public interface UserUseCase {
    UserResponse registerUser(UserRequest request);
    UserResponse loginUser(LoginRequest request);
    List<UserDto> findAll();
    UserResponse findById(String id);
    UserResponse deleteUser(String id);
    UserResponse updateUser(String id, UpdateUserRequest request);
}
