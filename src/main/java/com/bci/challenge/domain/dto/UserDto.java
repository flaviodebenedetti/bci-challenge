package com.bci.challenge.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {
    private String id;
    private String created;
    private String modified;
    private String lastLogin;
    private String token;
    private boolean isActive;
    private String email;
    private String password;
}
