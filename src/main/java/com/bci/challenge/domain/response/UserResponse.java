package com.bci.challenge.domain.response;

import com.bci.challenge.domain.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponse {
    private UserDto data;
    private String message;

    public static UserResponse registerOkResponse(UserDto userDto) {
        return UserResponse.builder()
                .data(userDto)
                .message("Usuario creado con éxito")
                .build();
    }

    public static UserResponse findUserOkResponse(UserDto userDto) {
        return UserResponse.builder()
                .data(userDto)
                .build();
    }

    public static UserResponse updateOkResponse() {
        return UserResponse.builder()
                .message("Usuario actualizado con éxito")
                .build();
    }

    public static UserResponse deleteOkResponse() {
        return UserResponse.builder()
                .message("Usuario eliminado con éxito")
                .build();
    }

    public static UserResponse loginOkResponse(String name) {
        return UserResponse.builder()
                .message("Bienvenid@ " + name + "!")
                .build();
    }

}
