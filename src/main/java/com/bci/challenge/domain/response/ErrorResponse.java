package com.bci.challenge.domain.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
    private String mensaje;

    public static ErrorResponse errorMessage(String mensaje) {
        return ErrorResponse.builder()
                .mensaje(mensaje)
                .build();
    }
}
