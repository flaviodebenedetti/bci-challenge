package com.bci.challenge.domain.entities;

import jakarta.persistence.*;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="telefonos")
public class PhoneEntity {
    @Id
    private Long id;
    private String number;
    private String citycode;
    private String countrycode;
}
