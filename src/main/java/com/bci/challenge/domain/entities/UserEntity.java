package com.bci.challenge.domain.entities;

import com.bci.challenge.domain.dto.UserDto;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="usuario", uniqueConstraints = {@UniqueConstraint(columnNames = {"email"})})
public class UserEntity {
    @Id
    private String id;
    private String name;
    private String email;
    private String password;
    private String created;
    private String modified;
    @Column(name = "last_login")
    private String lastLogin;
    private String token;
    @Column(name = "is_active")
    private boolean isActive;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<PhoneEntity> phones;

    public UserDto toUserDto(){
        return UserDto.builder()
                .id(id)
                .created(created)
                .modified(modified)
                .lastLogin(lastLogin)
                .token(token)
                .isActive(isActive)
                .build();
    }

    public UserDto toUserDtoAllAttributes(){
        return UserDto.builder()
                .id(id)
                .email(email)
                .password(password)
                .created(created)
                .modified(modified)
                .lastLogin(lastLogin)
                .token(token)
                .isActive(isActive)
                .build();
    }
}
