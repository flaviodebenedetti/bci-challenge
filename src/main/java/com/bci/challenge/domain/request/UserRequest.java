package com.bci.challenge.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;

import java.util.List;

@Builder
public record UserRequest(
        @JsonProperty("name")
        @NotEmpty(message = "Nombre requerido")
        String name,
        @JsonProperty("email")
        @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$", message = "Email con formato invalido")
        String email,
        @JsonProperty("password")
        @NotEmpty(message = "Contraseña requerida")
        String password,
        @JsonProperty("phones")
        @NotEmpty(message = "Datos requeridos")
        List<PhoneRequest> phones
){}
