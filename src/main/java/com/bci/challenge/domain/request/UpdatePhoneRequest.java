package com.bci.challenge.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public record UpdatePhoneRequest(
        @JsonProperty("number")
        String number,
        @JsonProperty("citycode")
        String citycode,
        @JsonProperty("countrycode")
        String countrycode
){}