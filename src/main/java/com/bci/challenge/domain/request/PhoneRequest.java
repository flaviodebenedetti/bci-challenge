package com.bci.challenge.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotEmpty;
import lombok.Builder;

@Builder
public record PhoneRequest(
        @JsonProperty("number")
        @NotEmpty(message = "Número requerido")
        String number,
        @JsonProperty("citycode")
        @NotEmpty(message = "Código postal requerido")
        String citycode,
        @JsonProperty("countrycode")
        @NotEmpty(message = "Código de país requerido")
        String countrycode
){}
