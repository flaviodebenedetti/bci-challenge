package com.bci.challenge.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Builder;

@Builder
public record LoginRequest(
        @JsonProperty("email")
        @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{3,}$", message = "Email con formato invalido")
        String email,
        @JsonProperty("password")
        @NotEmpty(message = "Password requerida")
        String password
){}
