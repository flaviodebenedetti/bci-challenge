package com.bci.challenge.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.List;

@Builder
public record UpdateUserRequest (
        @JsonProperty("name")
        String name,
        @JsonProperty("email")
        String email,
        @JsonProperty("password")
        String password,
        @JsonProperty("token")
        String token,
        @JsonProperty("is_active")
        Boolean isActive,
        @JsonProperty("phones")
        List<UpdatePhoneRequest> phones
){}
