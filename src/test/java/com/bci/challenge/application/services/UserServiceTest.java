package com.bci.challenge.application.services;

import com.bci.challenge.config.ValidationConfig;
import com.bci.challenge.domain.dto.UserDto;
import com.bci.challenge.domain.entities.UserEntity;
import com.bci.challenge.domain.request.LoginRequest;
import com.bci.challenge.domain.request.PhoneRequest;
import com.bci.challenge.domain.request.UpdateUserRequest;
import com.bci.challenge.domain.request.UserRequest;
import com.bci.challenge.domain.response.UserResponse;
import com.bci.challenge.infrastructure.exceptions.EmailAlreadyExistsException;
import com.bci.challenge.infrastructure.exceptions.FindUserByIdNotFoundException;
import com.bci.challenge.infrastructure.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceTest {

    @Mock
    private UserRepository repository;
    @Mock
    private ValidationConfig validationConfig;
    @InjectMocks
    private UserService userService;
    private PhoneRequest phoneRequest;
    private UserRequest userRequest;
    private LoginRequest loginRequest;
    private UpdateUserRequest updateUserRequest;
    private UserEntity userEntity;
    private List<UserEntity> userEntityList;

    @BeforeEach
    void setup(){
        phoneRequest = PhoneRequest.builder()
                .number("123")
                .citycode("123")
                .countrycode("123")
                .build();
        userRequest = UserRequest.builder()
                .email("test@test.com")
                .name("test")
                .password("P@sswordtest1")
                .phones(List.of(phoneRequest))
                .build();
        updateUserRequest = UpdateUserRequest.builder()
                .email("test@test.com")
                .name("test")
                .password("P@sswordtest1")
                .build();
        userEntity = UserEntity.builder()
                .name("Test")
                .id("testId")
                .email("test@test.com").build();
        userEntityList = new ArrayList<>();
        userEntityList.add(userEntity);
        userEntityList.add(userEntity);
        when(validationConfig.getPasswordRegex()).thenReturn("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=.])(?=\\S+$).{8,16}$");
    }

    @Test
    void whenRegisterUserIsOk(){
        when(repository.findByEmail(any())).thenReturn(List.of());

        UserResponse response = userService.registerUser(userRequest);

        Assertions.assertEquals("Usuario creado con éxito", response.getMessage());
    }

    @Test
    void whenRegisterUserWithEmailAlreadyExistsException(){
        List list = new ArrayList<>();
        list.add("");
        list.add("");
        list.add("");

        when(repository.findByEmail(any())).thenReturn(list);

        assertThrows(EmailAlreadyExistsException.class, () -> userService.registerUser(userRequest));
    }

    @Test
    void whenLoginUserIsOk(){
        givenLoginData();

        when(repository.findByEmailAndPassword(any(), any())).thenReturn(userEntity);

        UserResponse response = userService.loginUser(loginRequest);

        Assertions.assertEquals("Bienvenid@ Test!", response.getMessage());
    }

    @Test
    void whenLoginUserWithFindUserByIdNotFoundException(){
        givenLoginData();

        when(repository.findByEmailAndPassword(any(), any())).thenReturn(null);

        assertThrows(FindUserByIdNotFoundException.class, () -> userService.loginUser(loginRequest));
    }

    @Test
    void whenFindAllIsOk(){
        when(repository.findAll()).thenReturn(userEntityList);

        List<UserDto> users = userService.findAll();

        Assertions.assertEquals(false, users.isEmpty());
        Assertions.assertEquals(true, users.size() > 0);
    }

    @Test
    void whenFindAllWithFindUserByIdNotFoundException(){
        when(repository.findAll()).thenReturn(List.of());

        assertThrows(FindUserByIdNotFoundException.class, () -> userService.findAll());
    }

    @Test
    void whenFindByIdIsOk(){
        when(repository.findById(any())).thenReturn(Optional.of(userEntity));

        UserResponse response = userService.findById("testId");

        Assertions.assertEquals(userEntity.getId(), response.getData().getId());
    }

    @Test
    void whenFindByIdWithFindUserByIdNotFoundException(){
        when(repository.findById(any())).thenReturn(Optional.empty());

        assertThrows(FindUserByIdNotFoundException.class, () -> userService.findById("testId"));
    }

    @Test
    void whenDeleteByIdIsOk(){
        when(repository.findById(any())).thenReturn(Optional.of(userEntity));

        UserResponse response = userService.deleteUser("testId");

        Assertions.assertEquals("Usuario eliminado con éxito", response.getMessage());
    }

    @Test
    void whenDeleteByIdWithFindUserByIdNotFoundException(){
        when(repository.findById(any())).thenReturn(Optional.empty());

        assertThrows(FindUserByIdNotFoundException.class, () -> userService.deleteUser("testId"));
    }

    @Test
    void whenUpdateUserIsOk(){
        when(repository.findById(any())).thenReturn(Optional.of(userEntity));

        UserResponse response = userService.updateUser("testId", updateUserRequest);

        Assertions.assertEquals("Usuario actualizado con éxito", response.getMessage());
    }

    @Test
    void whenUpdateUserWithFindUserByIdNotFoundException(){
        when(repository.findById(any())).thenReturn(Optional.empty());

        assertThrows(FindUserByIdNotFoundException.class, () -> userService.updateUser("testId", updateUserRequest));
    }

    private void givenLoginData() {
        loginRequest = LoginRequest.builder()
                .email("test@test.com")
                .password("testpassword")
                .build();
    }

}
