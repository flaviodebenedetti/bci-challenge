# bci-challenge

## Descripción

Aplicación desarrollada con spring-boot, Java 17, gradle y H2 como motor de base de datos.
Dicha aplicación cumple con las funciones CRUD (GET, POST, UPDATE, DELETE)

## Modo de Uso

Una vez clonado el proyecto se puede ejecutar por consola con el siguiente comando:
``` 
cd bci-challenge
gradle bootRun
```

La aplicación ejecutará por default el archivo "data.sql" ubicado en el path "src/main/resources/". Esto insertará valores por defecto en nuestra base de datos H2.

En el mismo PATH se encuentra un archivo llamado "BCI.postman_collection.json" el cual es una collection de postman
con los endpoints de la aplicación.
Cada llamada a estos endpoint va a tener como respuesta un mensaje de error o un mensaje de éxito cada uno con su
status code correspondiente.

## Características

    - Registrar un nuevo usuario a la base de datos
    - Eliminar un usuario
    - Buscar por ID de usuario
    - Buscar todos los usuarios
    - Actualizar un usuario
    - Hacer un login

## Diagramas de Flujo

Los diagramas de flujo de cada caso de uso se encuentran en el PATH "src/main/resources/diagrams"

